#! /bin/sh

set -u

rt=/tmp/sollya_comparisons

mkdir "$rt"

for t in A1 A2; do
	mkdir "$rt"/"$t"_final

	for i in 0 1 2 3 4 5 6; do
		# Join CSVs.
		join -t , -1 1 -2 1 -o 0,1.2,1.3,2.3 "$rt"/"$t"/rand"$i"_*.csv > "$rt"/"$t"_final/rand"$i".csv

		# Make HTML Vega Lite visualizations from template files.
		< ./templates_html/A1A2.html.tmplt > "$rt"/"$t"_final/rand"$i".html sed -E -e '
		  s/A1A2/'"$t"'/g
		  s/RAND/rand'"$i"'/g'
	done
done
