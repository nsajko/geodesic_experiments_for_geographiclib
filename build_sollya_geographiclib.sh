#! /bin/dash

set -u

cxx_opt=
cxx_opt="$cxx_opt"' -std=c++20 -pedantic -Wpedantic -Werror -Wall -Wextra'
cxx_opt="$cxx_opt"' -Wcast-align -Wconversion -Wold-style-cast'
cxx_opt="$cxx_opt"' -g -march=native -flto -O3'
cxx_opt="$cxx_opt"' -I ./include'

bindir=/tmp/geo_sollya

mkdir "$bindir"

geographiclib_root=/home/nsajko/src/git.code.sf.net/p/geographiclib/code/geographiclib-code
geographiclib_include=include
geographiclib_so=build/src/libGeographic.so

for f in src_sollya_geographiclib/*.cc; do
	F="$(basename "$f" .cc)"
	g++ $cxx_opt -shared -fPIC -l gmp -l mpfr "$geographiclib_root"/"$geographiclib_so" -I "$geographiclib_root"/"$geographiclib_include" "$f" -o "$bindir"/"$F".so
done

for f in src_sollya_geographiclib/rand*.cc; do
	F="$(basename "$f" .cc)"
	< ./templates_sollya/comparison.sollya.tmplt sed -E -e '
	  s/RAND/'"$F"'/g' > "$bindir"/"$F"_comparison.sollya
done
