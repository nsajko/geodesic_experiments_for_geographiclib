#pragma once

// Copyright © 2021 Neven Sajko. All rights reserved.
//
// The aim is to have several random double generators, which together
// generate values from complete_interval.
//
// rand0 generates from complete_interval.first to shift_3_exponent_maximum.
//
// rand1 generates from shift_3_exponent_maximum to exponent_minima[5].
//
// rand2 generates from exponent_minima[5] to exponent_minima[4].
//
// rand3 generates from exponent_minima[4] to exponent_minima[3].
//
// rand4 generates from exponent_minima[3] to exponent_minima[2].
//
// rand5 generates from exponent_minima[2] to exponent_minima[1].
//
// rand6 generates from exponent_minima[1] to complete_interval.second.

#include <array>

namespace {

constexpr auto complete_interval{std::make_pair(0.0, 1.68487418556910874e-3)};

constexpr auto shift_3_exponent_maximum{1.4916681462400412e-154};
static_assert(shift_3_exponent_maximum == 0x1.fffffffffffffp-512);

constexpr auto exponent_minima{std::to_array({
	1.953125e-3,
	9.765625e-4,
	4.8828125e-4,
	2.44140625e-4,
	1.220703125e-4,
	6.103515625e-5,
})};
static_assert(exponent_minima[0] == 0x1p-9);
static_assert(exponent_minima[1] == 0x1p-10);
static_assert(exponent_minima[2] == 0x1p-11);
static_assert(exponent_minima[3] == 0x1p-12);
static_assert(exponent_minima[4] == 0x1p-13);
static_assert(exponent_minima[5] == 0x1p-14);

}  // namespace
