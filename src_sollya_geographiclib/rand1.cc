// Copyright © 2021 Neven Sajko. All rights reserved.
//
// See rand.h.

#include <mpfr.h>

#include "pseudorandom_generation_ieee_754.h"

#include "rand.h"

extern "C"
auto rand1(mpfr_t r) -> int {
	using namespace pseudorandom_generation_ieee_754;

	static auto prg{Xoshiro256PlusPlusConstantDefaultSeedIEEE754Double<
	  shift_3_exponent_maximum, exponent_minima[5], 2>{}};

	mpfr_set_d(r, prg.next(), MPFR_RNDN);

	return 1;
}
