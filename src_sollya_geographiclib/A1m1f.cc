// Copyright © 2021 Neven Sajko. All rights reserved.

#include <mpfi.h>
#include <GeographicLib/Geodesic.hpp>

#include "slfitfosa.h"

extern "C"
auto A1m1f_geolib(mpfi_t r, const mpfi_t x, int m) -> int {
	slfitfosa::function_1(GeographicLib::Geodesic::A1m1f,
	  r, x, m);
	return 0;
}
