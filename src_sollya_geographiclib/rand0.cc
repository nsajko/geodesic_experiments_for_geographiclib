// Copyright © 2021 Neven Sajko. All rights reserved.
//
// See rand.h.

#include <mpfr.h>

#include "pseudorandom_generation_ieee_754.h"

extern "C"
auto rand0(mpfr_t r) -> int {
	using namespace pseudorandom_generation_ieee_754;

	static auto prg{Xoshiro256PlusPlusConstantDefaultSeedIEEE754DoubleSimpleShift<
	  3>{}};

	mpfr_set_d(r, prg.next(), MPFR_RNDN);

	return 1;
}
