#pragma once

// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

namespace float_intervals {

// Checks if a and b are close. Clobbers diff_storage, which should
// have precision close to 1.
auto floats_are_close(const mpfr_t a, const mpfr_t b, mpfr_t diff_storage, long target) -> bool {
	const auto min_exp{std::min(mpfr_get_exp(a), mpfr_get_exp(b))};
	mpfr_sub(diff_storage, a, b, MPFR_RNDN);
	return mpfr_zero_p(diff_storage) || (mpfr_number_p(diff_storage) &&
	       target < min_exp - mpfr_get_exp(diff_storage));
}

auto float_is_one(const mpfr_t i) -> bool {
	return mpfr_cmp_si(i, 1) == 0;
}

auto interval_is_empty_or_nan(const mpfi_t i) -> bool {
	return !mpfr_lessequal_p(&(i->left), &(i->right));
}

auto interval_is_empty_and_not_nan(const mpfi_t i) -> bool {
	return mpfr_greater_p(&(i->left), &(i->right));
}

auto interval_set_empty(mpfi_t r) -> void {
	mpfr_set_inf(&(r->left), 1);
	mpfr_set_inf(&(r->right), -1);
}

auto interval_set_full_range(mpfi_t r) -> void {
	mpfr_set_inf(&(r->left), -1);
	mpfr_set_inf(&(r->right), 1);
}

auto interval_zero_sign_normalize(mpfi_t r) -> void {
	if (mpfr_zero_p(&(r->left))) {
		mpfr_set_zero(&(r->left), 1);
	}
	if (mpfr_zero_p(&(r->right))) {
		mpfr_set_zero(&(r->right), -1);
	}
}

auto interval_set_ui(mpfi_t r, unsigned long n) -> void {
	if (n == 0) {
		mpfr_set_zero(&(r->left), 1);
		mpfr_set_zero(&(r->right), -1);
		return;
	}

	mpfr_set_ui(&(r->left), n, MPFR_RNDD);
	mpfr_set_ui(&(r->right), n, MPFR_RNDU);
}

auto interval_add_ui(mpfi_t i, unsigned long n) -> void {
	mpfr_add_ui(&(i->left), &(i->left), n, MPFR_RNDD);
	mpfr_add_ui(&(i->right), &(i->right), n, MPFR_RNDU);
	interval_zero_sign_normalize(i);
}

auto interval_div_si(mpfi_t i, long n) -> void {
	if (n < 0) {
		mpfr_div_si(&(i->right), &(i->right), n, MPFR_RNDD);
		mpfr_div_si(&(i->left), &(i->left), n, MPFR_RNDU);
		mpfr_swap(&(i->left), &(i->right));
		return;
	}

	mpfr_div_ui(&(i->left), &(i->left), n, MPFR_RNDD);
	mpfr_div_ui(&(i->right), &(i->right), n, MPFR_RNDU);
}

auto interval_mul_si(mpfi_t i, long n) -> void {
	if (n < 0) {
		mpfr_mul_si(&(i->right), &(i->right), n, MPFR_RNDD);
		mpfr_mul_si(&(i->left), &(i->left), n, MPFR_RNDU);
		mpfr_swap(&(i->left), &(i->right));
		return;
	}

	mpfr_mul_ui(&(i->left), &(i->left), n, MPFR_RNDD);
	mpfr_mul_ui(&(i->right), &(i->right), n, MPFR_RNDU);
}

auto interval_mul_q(mpfi_t i, mpq_t q) -> void {
	if (mpq_sgn(q) < 0) {
		mpfr_mul_q(&(i->right), &(i->right), q, MPFR_RNDD);
		mpfr_mul_q(&(i->left), &(i->left), q, MPFR_RNDU);
		mpfr_swap(&(i->left), &(i->right));
		return;
	}

	mpfr_mul_q(&(i->left), &(i->left), q, MPFR_RNDD);
	mpfr_mul_q(&(i->right), &(i->right), q, MPFR_RNDU);
}

auto intervals2_mul(mpfi_t i, const mpfi_t x) -> void {
	mpfi_mul(i, i, x);
}

auto interval_get_prec(const mpfi_t i) -> long {
	return mpfr_get_prec(&(i->left));
}

auto interval_set_prec(mpfi_t i, long p) -> void {
	mpfr_set_prec(&(i->left), p);
	mpfr_set_prec(&(i->right), p);
}

auto interval_round_prec(mpfi_t i, long p) -> void {
	mpfr_round_prec(&(i->left), MPFR_RNDD, p);
	mpfr_round_prec(&(i->right), MPFR_RNDU, p);
}

auto intervals2_set(mpfi_t i, const mpfi_t x) -> void {
	mpfr_set(&(i->left), &(x->left), MPFR_RNDD);
	mpfr_set(&(i->right), &(x->right), MPFR_RNDU);
}

// Not robust.
auto float_interval_midpoint(mpfr_t i, const mpfi_t x) -> void {
	mpfr_add(i, &(x->left), &(x->right), MPFR_RNDN);
	mpfr_div_2ui(i, i, 1, MPFR_RNDN);
}

}  // namespace float_intervals
