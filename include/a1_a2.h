#pragma once

// Copyright © 2021 Neven Sajko. All rights reserved.

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "float_intervals.h"

namespace a1_a2 {

template<bool minus_one, int polynomial_degree, typename Numerator, typename Denominator>
auto modified_horner(mpfi_t r, const mpfi_t x) -> void {
	using namespace float_intervals;

	constexpr auto num{Numerator{}};
	constexpr auto den{Denominator{}};

	if constexpr (minus_one) {
		// Modified Horner polynomial evaluation without the
		// last addition of 1.
		for (auto i{polynomial_degree - 1}; 0 < i; i--) {
			intervals2_mul(r, x);
			interval_mul_si(r, num(i));
			interval_div_si(r, den(i));
			interval_add_ui(r, 1);
		}
		intervals2_mul(r, x);
		interval_mul_si(r, num(0));
		interval_div_si(r, den(0));
		return;
	}

	// Modified Horner polynomial evaluation.
	for (auto i{polynomial_degree - 1}; 0 <= i; i--) {
		intervals2_mul(r, x);
		interval_mul_si(r, num(i));
		interval_div_si(r, den(i));
		interval_add_ui(r, 1);
	}
}

template<int polynomial_degree, typename Numerator, typename Denominator>
auto modified_horner_derivatives(mpfi_t r, const mpfi_t x, int m) -> void {
	using namespace float_intervals;

	mpq_t clob;
	[](mpq_t q, int n) {
		constexpr auto num{Numerator{}};
		constexpr auto den{Denominator{}};

		mpq_init(q);
		mpq_set_ui(q, 1, 1);
		auto qnum{mpq_numref(q)};
		auto qden{mpq_denref(q)};

		for (int i{0}; i < n; i++) {
			mpz_mul_si(qnum, qnum, num(i));
			mpq_canonicalize(q);
			mpz_mul_ui(qnum, qnum, i + 1);
			mpq_canonicalize(q);
			mpz_mul_si(qden, qden, den(i));
			mpq_canonicalize(q);
		}
	}(clob, m);

	constexpr auto num{Numerator{}};
	constexpr auto den{Denominator{}};

	// Modified Horner polynomial evaluation.
	for (auto i{polynomial_degree - 1}; 0 <= i; i--) {
		intervals2_mul(r, x);
		interval_mul_si(r, num(i + m) * (i + 1 + m));
		interval_div_si(r, den(i + m) * (i + 1));
		interval_add_ui(r, 1);
	}

	interval_mul_q(r, clob);
	mpq_clear(clob);
}

template<bool minus_one, int polynomial_degree, int working_precision_factor, typename Numerator, typename Denominator>
auto modified_horner_function_or_derivative(mpfi_t r, const mpfi_t x, int m) -> void {
	using namespace float_intervals;

	// Sollya's manual states: For any function, if one of its
	// arguments is empty (respectively NaN), we return empty
	// (respectively NaN).
	if (interval_is_empty_or_nan(x)) {
		intervals2_set(r, x);
		return;
	}

	auto prec{interval_get_prec(r)};
	interval_set_prec(r, prec * working_precision_factor);

	// Multiplicative identity.
	interval_set_ui(r, 1);

	if (m == 0) {
		modified_horner<minus_one, polynomial_degree, Numerator, Denominator>(r, x);
	} else if (m == 1) {
		modified_horner_derivatives<polynomial_degree, Numerator, Denominator>(r, x, 1);
	} else if (m == 2) {
		modified_horner_derivatives<polynomial_degree, Numerator, Denominator>(r, x, 2);
	} else {
		modified_horner_derivatives<polynomial_degree, Numerator, Denominator>(r, x, m);
	}

	interval_round_prec(r, prec);
	interval_zero_sign_normalize(r);
}

using NumeratorA1 = decltype([](int i) {
	i = 2 * i - 1;
	return i * i;
});

using DenominatorA1 = decltype([](int i) {
	i = 2 * i + 2;
	return i * i;
});

using NumeratorA2 = decltype([](int i) {
	return NumeratorA1{}(i) * (4 * i + 3);
});

using DenominatorA2 = decltype([](int i) {
	return DenominatorA1{}(i) * (4 * i - 1);
});

template<bool minus_one, int polynomial_degree, int working_precision_factor>
auto function_A1(mpfi_t r, const mpfi_t x, int m) -> void {
	modified_horner_function_or_derivative<
		minus_one,
		polynomial_degree,
		working_precision_factor,
		NumeratorA1,
		DenominatorA1
	>(r, x, m);
}

template<bool minus_one, int polynomial_degree, int working_precision_factor>
auto function_A2(mpfi_t r, const mpfi_t x, int m) -> void {
	modified_horner_function_or_derivative<
		minus_one,
		polynomial_degree,
		working_precision_factor,
		NumeratorA2,
		DenominatorA2
	>(r, x, m);
}

}  // namespace a1_a2
