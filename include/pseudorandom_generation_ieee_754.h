#pragma once

// Copyright © 2021 Neven Sajko. All rights reserved.

#include <cstring>

#include "pseudorandom_generation.h"

namespace pseudorandom_generation_ieee_754 {

template<typename Xo, signed char shift>
class IEEE754DoublePRGSimpleShift {
	Xo prg{};

public:

	auto next() -> double {
		auto u{prg.next() >> shift};
		return std::bit_cast<double>(u);
	}
};

template<typename Xo, double lo, double hi, signed char shift>
class IEEE754DoublePRG {
	Xo prg{};

public:

	auto next() -> double {
		auto mantissa{prg.next() >> 12};
		for (;;) {
			auto u{(((prg.next() & 0xfffUL) >> shift) << 52) | mantissa};
			auto d{std::bit_cast<double>(u)};
			if (lo < d && d < hi) {
				return d;
			}
		}
	}
};

template<typename Xo, double sign_and_exp>
class IEEE754DoublePRGSimpleConstantSignExponent {
	Xo prg{};
	static constexpr uint64_t s_e{(std::bit_cast<uint64_t>(sign_and_exp) >> 52 ) << 52};

public:

	auto next() -> double {
		auto mantissa{((prg.next() << 12) >> 12)};
		return std::bit_cast<double>(s_e | mantissa);
	}
};

template<typename Xo, double lo, double hi, double sign_and_exp>
class IEEE754DoublePRGConstantSignExponent {
	Xo prg{};
	static constexpr uint64_t s_e{(std::bit_cast<uint64_t>(sign_and_exp) >> 52 ) << 52};

public:

	auto next() -> double {
		for (;;) {
			auto mantissa{((prg.next() << 12) >> 12)};
			auto d{std::bit_cast<double>(s_e | mantissa)};
			if (lo < d && d < hi) {
				return d;
			}
		}
	}
};

template<signed char shift>
using Xoshiro256PlusPlusConstantDefaultSeedIEEE754DoubleSimpleShift =
  IEEE754DoublePRGSimpleShift<
    pseudorandom_generation::XoshiroConstantWithStorage<
      pseudorandom_generation::Xoshiro256PlusPlusConstantDefaultSeed, 8192>,
    shift>;

template<double lo, double hi, signed char shift>
using Xoshiro256PlusPlusConstantDefaultSeedIEEE754Double =
  IEEE754DoublePRG<
    pseudorandom_generation::XoshiroConstantWithStorage<
      pseudorandom_generation::Xoshiro256PlusPlusConstantDefaultSeed, 8192>,
    lo, hi, shift>;

template<double sign_and_exp>
using Xoshiro256PlusPlusConstantDefaultSeedIEEE754DoubleSimpleConstantSignExponent =
  IEEE754DoublePRGSimpleConstantSignExponent<
    pseudorandom_generation::XoshiroConstantWithStorage<
      pseudorandom_generation::Xoshiro256PlusPlusConstantDefaultSeed, 8192>,
    sign_and_exp>;

template<double lo, double hi, double sign_and_exp>
using Xoshiro256PlusPlusConstantDefaultSeedIEEE754DoubleConstantSignExponent =
  IEEE754DoublePRGConstantSignExponent<
    pseudorandom_generation::XoshiroConstantWithStorage<
      pseudorandom_generation::Xoshiro256PlusPlusConstantDefaultSeed, 8192>,
    lo, hi, sign_and_exp>;

}  // namespace pseudorandom_generation_ieee_754
