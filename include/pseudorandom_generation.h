#pragma once

// Copyright © 2021 Neven Sajko. All rights reserved.

// C++20

// Pseudo random generators are by Blackman and Vigna (Xoshiro256++,
// Xoshiro256**, ...).
//
// They are strictly better than the generators offered in std, offering
// much better statistical quality with less state and with more
// throughput. AFAIK.
//
// Note that these:
//	* are not CSPRNGs
//	* do not provide any safety (a la Randen) against attackers

#include <array>
#include <bit>
#include <cstdint>

namespace pseudorandom_generation {

enum class XoshiroVariant {
	plus_plus,
	star_star,
};

// A PRNG, parameterized by its initial state.
//
// The first two parameters determine which of the Xoshiro variants
// will be used, while the rest of the parameters initialize the
// state.
template<int state_size, XoshiroVariant variant, uint64_t... initial_state>
requires ((state_size == 256 || state_size == 512) &&
          (sizeof...(initial_state) == (state_size / 64)) &&
          ((initial_state != 0) && ...))
class XoshiroConstant {
	// The PRNG state, initialized with constant random bits.
	std::array<uint64_t, sizeof...(initial_state)> s{initial_state...};

public:

	static constexpr auto xoshiro_info{std::make_pair(state_size, variant)};

	constexpr
	auto next(void) -> uint64_t {
		uint64_t ret;
		if constexpr (variant == XoshiroVariant::star_star) {
			ret = std::rotl(s[1] * 5, 7) * 9;
		}
		if constexpr (state_size == 256) {
			if constexpr (variant == XoshiroVariant::plus_plus) {
				ret = std::rotl(s[0] + s[3], 23) + s[0];
			}
	
			auto t{s[1] << 17};
	
			s[2] ^= s[0];
			s[3] ^= s[1];
			s[1] ^= s[2];
			s[0] ^= s[3];
	
			s[2] ^= t;
	
			s[3] = std::rotl(s[3], 45);
		} else if constexpr (state_size == 512) {
			if constexpr (variant == XoshiroVariant::plus_plus) {
				ret = std::rotl(s[0] + s[2], 17) + s[2];
			}
		
			auto t{s[1] << 11};
		
			s[2] ^= s[0];
			s[5] ^= s[1];
			s[1] ^= s[2];
			s[7] ^= s[3];
			s[3] ^= s[4];
			s[4] ^= s[5];
			s[0] ^= s[6];
			s[6] ^= s[7];
		
			s[6] ^= t;
		
			s[7] = std::rotl(s[7], 21);
		}
	
		return ret;
	}
};

// Xoshiro256++ parameterized by its initial state.
template<uint64_t InitialState0, uint64_t InitialState1, uint64_t InitialState2, uint64_t InitialState3>
using Xoshiro256PlusPlusConstant = XoshiroConstant<256, XoshiroVariant::plus_plus,
  InitialState0, InitialState1, InitialState2, InitialState3>;

constexpr auto xoshiro_default_initial_state{std::to_array<uint64_t>({
	0xdcbfcdafbe972023,
	0x6f496a9923b1364a,
	0xb3dcd40b7cd14da1,
	0xc658ab0e170a5d57,
	0x0da037571395e400,
	0x5be650169ba1dc4b,
	0xa7a50dc7c0c9c91b,
	0xbce420fa06c1c27f,
})};

// Xoshiro256++ with the default random constant seed.
using Xoshiro256PlusPlusConstantDefaultSeed = Xoshiro256PlusPlusConstant<
  xoshiro_default_initial_state[0],
  xoshiro_default_initial_state[1],
  xoshiro_default_initial_state[2],
  xoshiro_default_initial_state[3]>;

template<typename Xo, int storage_size>
class XoshiroConstantWithStorage {
	Xo prg{};
	int index{storage_size};
	std::array<uint64_t, storage_size> storage;

public:

	static constexpr auto xoshiro_info{Xo::xoshiro_info};
	static constexpr auto storage_size_info{storage_size};

	auto next(void) -> uint64_t {
		if (index == storage_size) {
			index = 0;
			for (auto i{0}; i < storage_size; i++) {
				storage[i] = prg.next();
			}
		}

		auto ret{storage[index]};
		index++;
		return ret;
	}
};

}  // namespace pseudorandom_generation
