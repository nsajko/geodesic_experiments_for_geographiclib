#pragma once

// Copyright © 2021 Neven Sajko. All rights reserved.
//
// Interfaces between Sollya library function interface and functions
// of one scalar argument.

#include <mpfr.h>
#include <mpfi.h>

#include "float_intervals.h"

namespace slfitfosa {

template<typename Fun>
auto function_1(Fun fun, mpfi_t r, const mpfi_t x, int m) -> void {
	using namespace float_intervals;

	// Sollya's manual states: For any function, if one of its
	// arguments is empty (respectively NaN), we return empty
	// (respectively NaN).
	if (interval_is_empty_or_nan(x)) {
		intervals2_set(r, x);
		return;
	}

	mpfr_t clob;
	mpfr_init2(clob, 1);

	if (m != 0 || !floats_are_close(&(x->left), &(x->right), clob, 16)) {
		interval_set_full_range(r);
		mpfr_clear(clob);
		return;
	}

	mpfr_clear(clob);

	float_interval_midpoint(&(r->left), x);

	auto y{fun(mpfr_get_d(&(r->left), MPFR_RNDN))};

	mpfr_set_d(&(r->left), y, MPFR_RNDD);
	mpfr_set_d(&(r->right), y, MPFR_RNDU);

	interval_zero_sign_normalize(r);
}

}  // namespace slfitfosa
