// Copyright © 2021 Neven Sajko. All rights reserved.

#include <iostream>

#include <boost/multiprecision/cpp_int.hpp>  // for A1, A2

#include "coefficients.h"  // for C1, C1p, C2

namespace {

using namespace coefficients;

auto print_rat(RationalNumberConstant q) {
	std::cout << q.first << '/' << q.second;
}

auto print_poly_truncated(const RationalNumberConstants &poly, int degree_truncated, std::string name, int poly_in_seq) -> void {
	auto truncated{degree_truncated != -1};

	std::cout << name << '_' << poly_in_seq;
	if (truncated) {
		std::cout << "_trunc" << degree_truncated;
	}
	std::cout << " = ";

	auto size{static_cast<int>(poly.size())};
	if (truncated) {
		if (size <= degree_truncated) {
			std::cout << "PANIC ABORT PANIC;\n";
			return;
		}
		size = degree_truncated + 1;
	}

	print_rat(poly[0]);

	for (auto j{1}; j < size; j++) {
		std::cout << " + x * (\n ";
		print_rat(poly[j]);
	}

	for (auto j{1}; j < size; j++) {
		std::cout << ')';
	}

	std::cout << ";\n";
}

auto print_fpminimax(std::string seq_name, int poly_in_seq, int max_degree) -> void {
	for (auto d{0}; d <= max_degree; d++) {
		std::cout << seq_name << '_' << poly_in_seq << "_fpmm" << d << " = fpminimax(" <<
		             seq_name << '_' << poly_in_seq << ", " << d << ", doubles, dom2);\n";
	}
}

auto print_poly(const RationalNumberConstants &poly, int max_truncated, std::string seq_name, int poly_in_seq) -> void {
	for (auto d{0}; d <= max_truncated; d++) {
		print_poly_truncated(poly, d, seq_name, poly_in_seq);
	}
	print_poly_truncated(poly, -1, seq_name, poly_in_seq);

	print_fpminimax(seq_name, poly_in_seq, max_truncated);
}

template<unsigned long n>
auto print(std::array<RationalNumberConstants, n> seq, std::string name, int max_truncated) -> void {
	for (auto i{1}; i <= static_cast<decltype(i)>(n); i++) {
		print_poly(seq[i - 1], max_truncated, name, i);
		std::cout << '\n';
	}
}

template<int polynomial_degree, typename Numerator, typename Denominator>
auto print_from_coefficient_factor_rat_formulas(std::string name) -> void {
	constexpr auto num{Numerator{}};
	constexpr auto den{Denominator{}};

	using Rat = boost::multiprecision::cpp_rational;

	std::cout << name << " = 0";

	auto coef{Rat{1}};

	for (auto j{0}; j < polynomial_degree; j++) {
		coef *= num(j);
		coef /= den(j);
		std::cout << " + x * (\n " << coef;
	}

	for (auto j{0}; j < polynomial_degree; j++) {
		std::cout << ')';
	}

	std::cout << ";\n";
}

auto print_sollya_header() -> void {
	std::cout << R"delim(x;

prec   = 512!;
points = 8192!;

procedure poly_coeffs(p) {
    var i, ret;
    ret = [| |];
    for i from degree(p) to 0 by -1 do {
        ret = ret :. coeff(p, i);
    };
    return ret;
};

singles := [|single...|];
doubles := [|double...|];

f    := 1 / 298.257223563;
f1   := 1 - f;
e2   := f * (2 - f);
ep2  := e2 / f1^2;
k2   = x * ep2;
eps  = k2(x) / (2 * (1 + sqrt(1 + k2(x))) + k2(x));
eps2 = eps(x)^2;
dom  := eps([0, 1]);
dom2 := eps2([0, 1]);

)delim";
}

auto print_sollya_footer() -> void {
	std::cout << R"delim(

A1m1f = (A1m1f_ser(x^2) + x) / (1 - x);
A2m1f = (A2m1f_ser(x^2) - x) / (1 + x);

A1m1f_fpmm5 = fpminimax(A1m1f, [|1, 2, 3, 4, 5|], doubles, dom);
A2m1f_fpmm5 = fpminimax(A2m1f, [|1, 2, 3, 4, 5|], doubles, dom);

print(poly_coeffs(A1m1f_fpmm5)) > "/tmp/sollya_result_A1m1f_fpmm5";
print(poly_coeffs(A2m1f_fpmm5)) > "/tmp/sollya_result_A2m1f_fpmm5";

print(poly_coeffs(C1_1_fpmm2)) >  "/tmp/sollya_result_C1_fpmm2";
print(poly_coeffs(C1_2_fpmm2)) >> "/tmp/sollya_result_C1_fpmm2";
print(poly_coeffs(C1_3_fpmm2)) >> "/tmp/sollya_result_C1_fpmm2";
print(poly_coeffs(C1_4_fpmm2)) >> "/tmp/sollya_result_C1_fpmm2";
print(poly_coeffs(C1_5_fpmm2)) >> "/tmp/sollya_result_C1_fpmm2";
print(poly_coeffs(C1_6_fpmm2)) >> "/tmp/sollya_result_C1_fpmm2";

print(poly_coeffs(C1p_1_fpmm2)) >  "/tmp/sollya_result_C1p_fpmm2";
print(poly_coeffs(C1p_2_fpmm2)) >> "/tmp/sollya_result_C1p_fpmm2";
print(poly_coeffs(C1p_3_fpmm2)) >> "/tmp/sollya_result_C1p_fpmm2";
print(poly_coeffs(C1p_4_fpmm2)) >> "/tmp/sollya_result_C1p_fpmm2";
print(poly_coeffs(C1p_5_fpmm2)) >> "/tmp/sollya_result_C1p_fpmm2";
print(poly_coeffs(C1p_6_fpmm2)) >> "/tmp/sollya_result_C1p_fpmm2";

print(poly_coeffs(C2_1_fpmm2)) >  "/tmp/sollya_result_C2_fpmm2";
print(poly_coeffs(C2_2_fpmm2)) >> "/tmp/sollya_result_C2_fpmm2";
print(poly_coeffs(C2_3_fpmm2)) >> "/tmp/sollya_result_C2_fpmm2";
print(poly_coeffs(C2_4_fpmm2)) >> "/tmp/sollya_result_C2_fpmm2";
print(poly_coeffs(C2_5_fpmm2)) >> "/tmp/sollya_result_C2_fpmm2";
print(poly_coeffs(C2_6_fpmm2)) >> "/tmp/sollya_result_C2_fpmm2";

)delim";
}

}  // namespace

auto main() -> int {
	std::ios::sync_with_stdio(false);

	print_sollya_header();
	std::cout << '\n';

	using NumeratorA1 = decltype([](int i) {
		i = 2 * i - 1;
		return i * i;
	});
	using DenominatorA1 = decltype([](int i) {
		i = 2 * i + 2;
		return i * i;
	});
	print_from_coefficient_factor_rat_formulas<
		127,
		NumeratorA1,
		DenominatorA1
	>("A1m1f_ser");
	std::cout << '\n';

	using NumeratorA2 = decltype([](int i) {
		return NumeratorA1{}(i) * (4 * i + 3);
	});
	using DenominatorA2 = decltype([](int i) {
		return DenominatorA1{}(i) * (4 * i - 1);
	});
	print_from_coefficient_factor_rat_formulas<
		127,
		NumeratorA2,
		DenominatorA2
	>("A2m1f_ser");
	std::cout << '\n';

	print(C1, "C1", 2);
	std::cout << '\n';

	print(C1p, "C1p", 2);
	std::cout << '\n';

	print(C2, "C2", 2);
	std::cout << '\n';

	print_sollya_footer();
}
