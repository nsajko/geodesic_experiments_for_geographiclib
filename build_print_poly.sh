#! /bin/dash

set -u

cxx_opt=
cxx_opt="$cxx_opt"' -std=c++20 -pedantic -Wpedantic -Werror -Wall -Wextra'
cxx_opt="$cxx_opt"' -Wcast-align -Wconversion -Wold-style-cast'
cxx_opt="$cxx_opt"' -g -march=native -flto -O3'
cxx_opt="$cxx_opt"' -I ./include'

bindir=/tmp/geo_print_poly

mkdir "$bindir"

for f in src_print_poly/*.cc; do
	F="$(basename "$f" .cc)"
	g++ $cxx_opt "$f" -o "$bindir"/"$F"
done
