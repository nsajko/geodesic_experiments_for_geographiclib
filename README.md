# What is this?

This repository is meant to accompany a Sourceforge merge request to CFF Karney's GeographicLib. What GeographicLib does, among other things, is compute geodesics on an ellipsoid of revolution. This is, I believe, almost always used with a single specific ellipsoid of revolution, WGS84, which approximates Earth. However, GeographicLib is built in such a way that it could be used for an arbitrary ellipsoid of revolution, and currently it does no optimization for the common case.

GeographicLib has implementations in several different languages, my focus is C++.

The aim of the changes I propose for GeographicLib is to optimize for the common case where computation is done on the WGS84 ellipsoid using IEEE 754 binary64 (double in C and C++). More precisely, my aim was to improve the (already great) accuracy, but the changes should also be beneficial for execution speed (although I didn't check that).

To achieve this, I focused on improving (given the restriction described above) several GeographicLib functions. Currently these are `Geodesic::A1m1f`, `Geodesic::A2m1f`, `Geodesic::C1f`, `Geodesic::C1pf` and `Geodesic::C2f`; but the principles can be applied to some other GeographicLib functions, too. The idea is to approximate more complicated mathematical functions with good low-degree polynomials.

So this repo is basically meant to enable anyone to reproduce my results.

## Some relevant links

* GeographicLib:
    * https://geographiclib.sourceforge.io
    * https://sourceforge.net/p/geographiclib/code/ci/devel/tree/
* Karney's papers: https://geographiclib.sourceforge.io/geod.html
    * DOI of the published paper: 10.1007/s00190-012-0578-z

## External dependencies

Both dependencies are optional, because important results are checked into Git.

Sollya is used for finding the polynomial approximations and for calculating relative errors in high precision:

* https://www.sollya.org
* https://gitlab.inria.fr/sollya/sollya
* https://www.sollya.org/sollya-weekly/sollya.php

Fricas (the Guess, GuessInteger and related packages to be more specific) is used for finding the formula for a sequence given some sample elements. (Currently this is applied only to A1 and A2, for which Fricas finds nice, simple formulas.):

* https://fricas.github.io

## Getting the truncated series

1. Modify maxima/geod.mac from the GeographicLib source:
    * Hardcode maxpow (I used the value 77).
    * Remove irrelevant functions and calls.
    * Use `compile(all);`.
    * Just call `computeall();` and `dispseries();` in the end.
2. Run the Maxima script. If using SBCL for running Maxima, it is probably necessary to increase the limit on heap usage with `--dynamic-space-size`:
    * `maxima -X '--dynamic-space-size 30720' -b geod.mac > geod77.txt`
    * This can run for a couple of days. With maxpow hardcoded to 77, the previous command ran for 77.6 hours. Hardcode maxpow to less than 77 if you're not comfortable with that.
3. Produce the files A1, A2, C1, C1p and C2 from geod77.txt. They should include the dispseries output lines for the corresponding series. I stored mine to the maxima directory in this repo.

## Getting the truncated series coefficients as Fricas Input code

In ./maxima, run `./make_fricas_dot_input.sh`. The produced .input file can then be used with Fricas for getting the formulas for an arbitrary coefficient of the A1 or A2 series.

## Getting the truncated series coefficients as C++

The Unix shell script make_c++.sh produces a C++ header file on stdout when run as `./make_c++.sh` from the directory that contains C1, C1p and C2.

I stored the file produced this way as coefficients.h in the src_print_poly directory, where it is used by print_poly_from_coefficients.cc (build it with `./build_print_poly.sh`) for generating a Sollya script which defines the A1m1f, A2m1f functions and the C1, C1p and C2 truncated series, and computes the polynomial approximations. The code that searches for max relative errors expects this Sollya script to be available on /tmp/polynomials.sollya.

## Finding the polynomial approximations with Sollya

Store the Sollya script mentioned above to /tmp/polynomials.sollya. It can be executed like this `sollya /tmp/polynomials.sollya`, which will produce coefficients in separate files under the /tmp directory. Another option is to start Sollya, and then command it thus: `execute("/tmp/polynomials.sollya");`. This leaves the Sollya REPL open after it executes, so it's possible to inspect the variables, plot with Gnuplot, export some other values, etc.

## Postprocessing the Sollya-produces coefficient lists

Running `./postprocess_sollya_polynomial_coefficients.sh` changes the files under `/tmp/sollya_result_*` to be more amenable to copying into GeographicLib C++ sources.

## Quantifying the improvements

What I would really have liked at this point is a comparison of maximum relative error of each changed function on the GeographicLib devel Git branch, versus on my nsajko-polynomials Git branch. However there are too many possible double/binary64 values for an exhaustive search for the maximum relative error to be possible. So what I did instead is:

* Take a generator of random binary64 values from the interval that is the domain of the relevant function.
* Choose a large natural number, I used 2^23, we can call it `n`.
* Start taking samples using the random binary64 generator, keeping track of the maximum relative error. Each time after taking a sample check whether the current number of samples is a power of two. If it is, record the current maximum.
* When the current number of samples reaches `n`, we are finished.
* At the end we have an increasing sequence of maximal errors, which we hope trends and other characteristics will be obvious from. Basically, I want to compare these sequences (graphically, plotted) instead of the (unattainable) actual maximal errors.

Another thing is that I divided the domain into 7 sub-intervals, and each interval has its own pseudo-randomness generator used for drawing binary64 values. They are named from rand0 to rand6:

* rand0 generates from 0 to about 1.5e-154.
* rand1 generates from about 1.5e-154 to about 6.1e-5.
* rand2 generates from about 6.1e-5 to about 1.2e-4.
* rand3 generates from about 1.2e-4 to about 2.4e-4.
* rand4 generates from about 2.4e-4 to about 4.9e-4.
* rand5 generates from about 4.9e-4 to about 9.8e-4.
* rand6 generates from about 9.8e-4 to about 1.7e-3.

Run `./build_sollya_geographiclib.sh`. This creates the 7 random generators, 7 corresponding Sollya scripts, and the interface from GeographicLib's functions to Sollya. Execute (with the `execute` Sollya function) each script. Rand0 takes the longest to execute, the other scripts may be done much sooner. After a script is done executing, it is possible to store the results into a CSV file using the command `dumb_max_csv(dm1, dm2, "GeographicLib_devel");` for old code, or `dumb_max_csv(dm1, dm2, "nsajko-polynomials");` for new code. Before running this command within Sollya, ensure the existence of the directory /tmp/sollya_comparisons and sub-directories A1 and A2 within it.

Now run `./sollya_comparisons_mkviz.sh` to create pretty HTML plots that compare the old and the new code. To view them in a Web browser, it's first necessary to serve them (together with their CSV data files), this can easily (assuming Python is installed) be done with a command like `python3 -m http.server` run in the /tmp/sollya_comparisons directory. A backup of my relative error results is available under ./results_rel_error, so changing to that directory, doing `python3 -m http.server`, and copying the output URL to the browser address bar should make the HTML file visualizations available.

### Summary of the results (take a look at the plots, too)

Note that currently I only did this for A1m1f and A2m1f.

Firstly I must confess that there's one thing with the max relative error results that I don't understand at all. The intervals from rand1 to rand6 produce sensible and understandable plots, but the relative errors over rand0 are weird in two ways:

1. They have about the same orders of magnitude as their input values, 10^-155. I would have thought that it isn't possible for a binary64 algorithm to have a relative error so low, unless it's exactly zero.
2. They are the same for the old and the new code. No idea how this is possible.

To be honest I am perplexed by this, I guess it points to either me doing something wrong, MPFR doing something wrong, Sollya doing something wrong; or, simply something that I don't understand happening. I'm hoping that Charles Karney will be able to clarify this.

In any case, for intervals between rand1 and rand6, and especially for A2m1f, the results are good:

| A1m1f | old relative error | new relative error |
|-------|--------------------|--------------------|
| rand0 | 1.87e-154          | 1.87e-154          |
| rand1 | 2.65e-16           | 2.3e-16            |
| rand2 | 2.7e-16            | 2.55e-16           |
| rand3 | 2.7e-16            | 2.6e-16            |
| rand4 | 2.7e-16            | 2.55e-16           |
| rand5 | 2.72e-16           | 2.56e-16           |
| rand6 | 2.71e-16           | 2.51e-16           |

| A2m1f | old relative error | new relative error |
|-------|--------------------|--------------------|
| rand0 | 3.73e-155          | 3.73e-155          |
| rand1 | 3.2e-16            | 1.7e-16            |
| rand2 | 3.3e-16            | 1.7e-16            |
| rand3 | 3.3e-16            | 1.7e-16            |
| rand4 | 3.2e-16            | 1.7e-16            |
| rand5 | 3.2e-16            | 1.7e-16            |
| rand6 | 3.3e-16            | 1.7e-16            |

# Other goodies

The include/a1_a2.h header implements arbitrary precision interval arithmetic (MPFI, MPFR) polynomials of arbitrary degree for approximating the A1 and A2 functions, using the formulas for arbitrary coefficients.

# Appendix: Formulas for coefficients of some series

## A1

The series:

```
A1 * (1 - eps)
  = sum(coeff_A1(i) * (eps^2)^i, i = 0 .. %plusInfinity)
```

Fricas produces the following formula for an arbitrary coefficient, using the guess$GuessInteger function:

```
coeff_A1(m)
  = product((4 * i^2 - 4 * i + 1) / (4 * i^2 + 8 * i + 4), i = 0 .. (m - 1))
  = product((2 * i - 1)^2 / (2 * i + 2)^2, i = 0 .. (m - 1))
  = product(((2 * i - 1) / (2 * i + 2))^2, i = 0 .. (m - 1))
  = product((2 * i - 1) / (2 * i + 2), i = 0 .. (m - 1))^2
```

Note that the formula for `coeff_A1(m) / coeff_A1(m - 1)` is obvious from the above formula, but another way to get it is using the Fricas guessPRec$GuessInteger function. This ratio being easy to calculate enables us to express the truncated series in a modified Horner form:

```
a1(i)
  := ((2 * i - 1) / (2 * i + 2))^2

x := eps^2

for some natural number k, the degree of the polynomial in x:
sum(coeff_A1(i) * (eps^2)^i, i = 0 .. k)
  = 1 + a1(0)*x*(1 + a1(1)*x*(1 + a1(2)*x*(...(1 + a1(k - 1)*x)...)))
```

## A2

The series:

```
A2 * (1 + eps)
  = sum(coeff_A2(i) * (eps^2)^i, i = 0 .. %plusInfinity)
```

Fricas produces the following formula for an arbitrary coefficient, using the guess$GuessInteger function:

```
coeff_A2(m)
  = product((16 * i^3 - 4 * i^2 - 8 * i + 3) / (16 * i^3 + 28 * i^2 + 8 * i - 4), i = 0 .. (m - 1))
  = product(((2 * i - 1)^2 * (4 * i + 3)) / ((2 * i + 2)^2 * (4 * i - 1)), i = 0 .. (m - 1))
  = product(((2 * i - 1) / (2 * i + 2))^2 * (4 * i + 3) / (4 * i - 1), i = 0 .. (m - 1))
  = product((2 * i - 1) / (2 * i + 2), i = 0 .. (m - 1))^2 * product((4 * i + 3) / (4 * i - 1), i = 0 .. (m - 1))
```

Again the formula for `coeff_A2(m) / coeff_A2(m - 1)` is obvious and we can use a modified Horner's method to approximate the series.

```
a2(i)
  := ((2 * i - 1) / (2 * i + 2))^2 * (4 * i + 3) / (4 * i - 1)

x := eps^2

for some natural number k, the degree of the polynomial in x:
sum(coeff_A2(i) * (eps^2)^i, i = 0 .. k) = 1 + a2(0)*x*(1 + a2(1)*x*(1 + a2(2)*x*(...(1 + a2(k - 1)*x)...)))
```

## C1

C1 is a sequence of series. The k-th series is C1(k) (the first series is C1(1)):

```
C1(k)
  = eps^k * sum(coeff_C1(k, i) * (eps^2)^i, i = 0 .. %plusInfinity)
```

I wasn't able to find a formula for the coefficients.

## Derivatives

For each user-supplied library function, Sollya also needs its derivatives of all orders.

Take `x := eps^2`.

### A1

The m-th derivative of `A1 * (1 - eps)` with respect to x:

```
sum(coeff_A1(i) * (i * (i - 1) * ... * (i - m + 1)) * x^(i - m), i = m .. %plusInfinity)
```

This is again approximated with a polynomial of degree k:

```
poly_A1(x, m, k)
  = sum(coeff_A1(i) * (i * ... * (i - m + 1)) * x^(i - m), i = m .. k + m - 1)
  = sum(coeff_A1(i + m) * ((i + m) * ... * (i + 1)) * x^i, i = 0 .. k)
  = sum(coeff_A1(i + m) * ((i + m)! / i!) * x^i, i = 0 .. k)
  = coeff_A1(m) * m! * sum(coeff_A1(i + m) / coeff_A1(m) * ((i + m)! / i!) / m! * x^i, i = 0 .. k)
  = coeff_A1(m) * m! * sum(product(a1(m + r - 1) * (m + r) / r, r = 1 .. i) * x^i, i = 0 .. k)
  = coeff_A1(m) * m! * (1 + a1(m) * (m + 1) / 1 * x + ... + product(a1(m + r - 1) * (m + r) / r, r = 1 .. k) * x^k)
  = coeff_A1(m) * m! * (1 + a1(m) * (m + 1) / 1 * x * (1 + ... * (1 + a1(m + k - 1) * (m + k) / k * x)...))
```

### A2

Mostly the same as with A1, replace `coeff_A1` with `coeff_A2` and `a1` with `a2`.
